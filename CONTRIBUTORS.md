# The Codeberg Documentation Contributors

In alphabetic order (by last name / username):

- Martijn de Boer (@sexybiggetje)
- Christian Buhtz (@buhtz)
- Ivan Calandra (@ivan-paleo)
- Ben Cotterell (@benc)
- Lucas Hinderberger (@lhinderberger)
- @mray (for the Codeberg Logo)
- @n
- Holger Waechtler (@hw)
