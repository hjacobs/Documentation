---
eleventyNavigation:
  key: ThirdPartyTools
  title: Including images in wiki pages
  parent: AdvancedUsage
---

## Upload images via git

Currently, gitea supports only images in the base directory of the wiki git. (Gitea wiki pages are stored in a separate wiki right beside the project, wiki pages themselves are markdown files).

A feasible workflow might look like this (replace `<user>` and `<project>` with your user and project name):

```shell
git clone git@codeberg.org:<user>/<project>.wiki.git
cd <project>.wiki.git
## now copy image file into this folder
git add <codeberg.png>
git commit -m "add image"
git push
```

Now, you can reference the image in markdown, like this:

```markdown
![image alt text](codeberg.png "image title")
```

After saving your changes, the image should be visible.

*NOTE: In contrast to embedding external images, images in git are only rendered after saving the wiki or markdown file changes.*
