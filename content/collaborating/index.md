---
eleventyNavigation:
  key: Collaborating
  title: Collaborating with Others
  icon: user-friends
  order: 30
---

These documentation pages contain detailed information on how you can collaborate 
on Codeberg, for example [by making Pull Requests](/collaborating/pull-requests-and-git-flow).

If you're new to Codeberg or software forges in general, please also have a look at
our [Getting Started Guide](/getting-started) which will teach you the basics of
issue tracking and wikis on Codeberg.