---
eleventyNavigation:
  key: CitableCode
  title: Citable Code
  parent: Collaborating
---

The easiest way to refer to your code is to send/post the URL of your repository. But if you need to reliably refer to a specific version of your code in the long run, for example for scientific writing, this is not really viable, because URLs can change.  
Another possibility is to download your repository as a ZIP or TAR.GZ archive (see section Clone in [Clone & Commit via Web](/git/clone-commit-via-web)) and share this file.  
The third option, which actually complements the second, is to assign a [Digital Object Identifier](https://www.doi.org/) (DOI) to your code. Having a *DOI* for your code means that everybody can cite your code using this *standard* and *permanent identifier*. Assigning a DOI for citation is routine in the academic world. 

This page will show you how to do just that. The process can be decomposed in four steps.

## Create a release of your repository 
Creating a release is optional but recommended. The reason is simply that a release will make it explicit what version of your code you want to share/cite. Any further edits to your code will not be included in the release. In other words, it is good practice to share/cite a release with fixed code rather than the always changing code of your repository.

To create a release, first go to the `Releases` tab of your repository, and click on `New Release`:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/releases1.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/releases1.png" alt="releases1">
</picture>

In the new page, enter a version number for your new release, select the branch that contains the code you want to release, add a title and a description:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/new-release.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/new-release.png" alt="new-release">
</picture>

You can either save as a draft, or publish the release outright.  

You are then re-directed to the `Releases` tab of your repository. The newly created release is now listed there:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/releases2.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/releases2.png" alt="releases2">
</picture>

Here you can edit the release if needed, and you can also download the source code in ZIP or TAR.GZ format.

## Download the ZIP archive
If you did create a release, download the ZIP archive of the source code on the `Releases` tab of your repository, as shown in the previous screenshot.

If not, you can simply download the ZIP archive of the repository (see section Clone in [Clone & Commit via Web](/git/clone-commit-via-web)).

## Upload the ZIP archive to an online repository
There are many online repositories made specifically for research, e.g. [Zenodo](https://zenodo.org/), [Figshare](https://figshare.com/), [Dryad](https://datadryad.org/), [OSF](https://osf.io/)... Others might be more appropriate for other uses.  

Upload the ZIP archive to one of them and follow the steps there to assign a DOI. Be sure that you add the URL of your Codeberg repository in the information of the record on the online repository, so that everyone accessing the record can easily find your Codeberg repository (and you!). In general, add as much information as applicable. 

> It should be possible to write an integration between Codeberg and one of the DOI repositories to make citing code a lot smoother. If you’re interested, you may want to have a look at [#295](https://codeberg.org/Codeberg/Community/issues/295).

## Share the DOI
Finally, get the DOI from the online repository. This is the permanent URL you can share and use for citation in scientific writing.
 
A DOI has the form `https://doi.org/repoID/recordID`. It has three parts:
* `https://doi.org`, the *proxy* is always there as is. It is sometimes omitted for compactness.
* `repoID`, the *prefix* is specific to the online repository. Its format is usually `10.XXXX`.
* `recordID`, the *suffix* identifies your record. Its format is specific to the online repository.
