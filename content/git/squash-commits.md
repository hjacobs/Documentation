---
eleventyNavigation:
  key: SquashCommits
  title: Merge multiple commits into one
  parent: Git
  order: 50
---

Sometimes you will merge multiple commits into one. Maybe the commits are "dirty" full with not working code or embarrasing commit messages. This solution is only one of mutliple possible solutions. See this [stackoverflow question](https://stackoverflow.com/q/2563632/4865723) for more details and variants.

Here is an example.

```bash
$ git log --graph --decorate --oneline
* cf634bb (HEAD -> master) english
* 722a9c7 zwei
* e59e6d0 eins
* c6990ba a
* 6dfc50b (origin/master, origin/HEAD) ix
* 10074d7 (tag: test) test
* 662e04e Initial commit

$ git status
Auf Branch master
Ihr Branch ist 4 Commits vor 'origin/master'.
  (benutzen Sie "git push", um lokale Commits zu publizieren)

nichts zu committen, Arbeitsverzeichnis unverändert
```

You want to merge the last 4 commits from `a` to `english`.

```bash
$ git reset --soft "HEAD~4"
$ git status
Auf Branch master
Ihr Branch ist auf demselben Stand wie 'origin/master'.

Zum Commit vorgemerkte Änderungen:
  (benutzen Sie "git reset HEAD <Datei>..." zum Entfernen aus der Staging-Area)

	neue Datei:     a

$ git commit --amend
[master 24e0e06] English
 Date: Wed May 27 13:56:28 2020 +0200
 2 files changed, 3 insertions(+), 1 deletion(-)
 create mode 100644 a

$ git log --graph --decorate --oneline
* 24e0e06 (HEAD -> master) English
* 10074d7 (tag: test) test
* 662e04e Initial commit
```

After that you can push it to remote repository via `git push`.