---
eleventyNavigation:
  key: CloneCommitViaSSH
  title: Clone & Commit via SSH
  parent: Git
  order: 40
---

> **Warning** Please make sure that before connecting to Codeberg via SSH,
> you have [verified Codeberg's SSH fingerprint](/security/ssh-fingerprint)!

The user in this examples is `JohnDoe` and it's repository is `foobar`. The repository was created via the Codeberg.org website including a `README.md` file.

```bash
~$ git clone git@codeberg.org:JohnDoe/foobar.git
Cloning into 'foobar'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
```

## Modify and commit to local repository
Modify an existing file

```bash
~$ cd foobar
~/foobar$ vim README.md
```

Commit changes to local repository.

```bash
~/foobar$ git commit -am 'test'
[master 10074d7] test
 1 file changed, 2 insertions(+), 1 deletion(-)
```

## Synchronize your local repository with the remote one
```bash
~/foobar$ git push
Username for 'https://codeberg.org': JohnDoe
Password for 'https://JohnDoe@codeberg.org': 
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 266 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To https://codeberg.org/JohnDoe/foobar.git
   662e04e..10074d7  master -> master
```